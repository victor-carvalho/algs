#! /usr/bin/env sh

GRID_SIZE=100
REPETITIONS=1000

echo "C++"
../xtime.rb ./perc_cpp $GRID_SIZE $REPETITIONS

echo
echo "Java"
../xtime.rb java Main $GRID_SIZE $REPETITIONS

echo
echo "Go"
../xtime.rb ./perc_go $GRID_SIZE $REPETITIONS

echo
echo "Ruby"
../xtime.rb ruby perc.rb $GRID_SIZE $REPETITIONS

echo
echo "Python"
../xtime.rb python perc.py $GRID_SIZE $REPETITIONS
