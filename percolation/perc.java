import java.util.Random;

class UnionFind {
    int[] id;
    int[] sz;

    UnionFind(int n) {
        id = new int[n];
        sz = new int[n];
        for (int i = 0; i < n; id[i] = i, i++);
        for (int i = 0; i < n; sz[i] = 1, i++);
    }
    boolean connected(int i, int j) {
        return root(i) == root(j);
    }
    void connect(int i, int j) {
        int p = root(i);
        int q = root(j);
        if (sz[p] < sz[q]) {
            id[p] = q;
            sz[q] += 1;
        }
        else {
            id[q] = p;
            sz[p] += 1;
        }
    }

    private int root(int i) {
        while (i != id[i]) {
            id[i] = id[id[i]];
            i = id[i];
        }
        return i;
    }
};

class Stats {
    static double sum(double[] xs) {
        double s = 0;
        for(int i = 0; i < xs.length; i++)
            s += xs[i];
        return s;
    }

    static double mean(double[] xs) {
        return sum(xs) / xs.length;
    }

    static double varp(double[] xs, double avg) {
        double sum = 0;
        for (int i = 0; i < xs.length; i++)
            sum += (xs[i] - avg) * (xs[i] - avg);
        return sum / xs.length;
    }

    static double stddevp(double[] xs, double avg) {
        return Math.sqrt(varp(xs, avg));
    }
};

class Percolation {
    UnionFind uf;
    boolean[][] data;
    int size, top, bot;

    Percolation(int n) {
        size = n;
        data = new boolean[size][size];
        top = size * size;
        bot = top + 1;
        uf = new UnionFind(top+2);
        for (int i = 0; i < size; i++)
          for (int j = 0; j < size; j++)
            data[i][j] = false;
        for (int i = 0; i < size; i++) uf.connect(i, top);
        for (int i = top - size; i < top; i++) uf.connect(i, bot);
    }

    void open(int i, int j) {
        int p = get(i, j);
        data[i-1][j-1] = true;

        if (i > 1 && isOpen(i-1, j))
            uf.connect(p, get(i-1, j));
        if (i < size && isOpen(i+1, j))
            uf.connect(p, get(i+1, j));
        if (j > 1 && isOpen(i, j-1))
            uf.connect(p, get(i, j-1));
        if (j < size && isOpen(i, j+1))
            uf.connect(p, get(i, j+1));
    }
    boolean isOpen(int i, int j) {
        return data[i-1][j-1];
    }
    boolean isFull(int i, int j) {
        return isOpen(i, j) && uf.connected(get(i, j), top);
    }
    boolean percolates() {
        return uf.connected(top, bot);
    }

    private int get(int i, int j) {
        if (i < 1 || i > size)
            throw new RuntimeException("Arguments must be between 1 and " + size);
        if (j < 1 || j > size)
            throw new RuntimeException("Arguments must be between 1 and " + size);
        return (i-1) * size + j - 1;
    }
};

class PercolationStats {
    double[] data;
    double m, s, cLo, cHi;

    PercolationStats(int n, int t) {
      data = new double[t];
      Random random = new Random();
      int total = n * n;
      for (int k = 0; k < t; k++) {
          int count = 0;
          Percolation p = new Percolation(n);
          while (!p.percolates()) {
              int i = (random.nextInt(n) + 1) % n + 1;
              int j = (random.nextInt(n) + 1) % n + 1;
              if (!p.isOpen(i, j)) {
                  p.open(i, j);
                  count += 1;
              }
          }
          data[k]= ((double) count) / total;
      }
      m = Stats.mean(data);
      s = Stats.stddevp(data, m);
      cLo = m - 1.96 * s;
      cHi = m + 1.96 * s;
    }

    double mean () {
        return m;
    }

    double stddev() {
        return s;
    }

    double confidenceLo() {
        return cLo;
    }

    double confidenceHi() {
        return cHi;
    }
};

class Main {
  public static void main(String[] args)
  {
      if (args.length == 2) {
          int n = Integer.parseInt(args[0]);
          int t = Integer.parseInt(args[1]);
          PercolationStats ps = new PercolationStats(n, t);
          System.out.println("mean                    = " + ps.mean());
          System.out.println("stddev                  = " + ps.stddev());
          System.out.println("95% confidence interval = (" + ps.confidenceLo() + ", " + ps.confidenceHi() + ")");
      }
  }
}
