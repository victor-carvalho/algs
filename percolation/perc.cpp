#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class UnionFind {
    vector<int> id;
    vector<int> sz;
public:
    UnionFind(int n) {
        sz.resize(n, 1);
        for (int i = 0; i < n; id.push_back(i), i++);
    }
    bool connected(int i, int j) {
        return root(i) == root(j);
    }
    void connect(int i, int j) {
        int p = root(i);
        int q = root(j);
        if (sz[p] < sz[q]) {
            id[p] = q;
            sz[q] += 1;
        }
        else {
            id[q] = p;
            sz[p] += 1;
        }
    }
private:
    int root(int i) {
        while (i != id[i]) {
            id[i] = id[id[i]];
            i = id[i];
        }
        return i;
    }
};

class Stats {
public:
    static double sum(vector<double> &xs) {
        double s = 0;
        for(size_t i = 0; i < xs.size(); i++)
            s += xs[i];
        return s;
    }

    static double mean(vector<double> &xs) {
        return ((double) sum(xs)) / xs.size();
    }

    static double varp(vector<double> &xs, double avg) {
        double sum = 0;
        for (size_t i = 0; i < xs.size(); i++)
            sum += (xs[i] - avg) * (xs[i] - avg);
        return sum / xs.size();
    }

    static double stddevp(vector<double> &xs, double avg) {
        return sqrt(varp(xs, avg));
    }
};

class Percolation {
    vector< vector<bool> > data;
    UnionFind *uf;
    int size, top, bot;
public:
    Percolation(int n) {
        size = n;
        data.resize(size, vector<bool>(size, false));
        top = size * size;
        bot = top + 1;
        uf = new UnionFind(top+2);
        for (int i = 0; i < size; i++) uf->connect(i, top);
        for (int i = top - size; i < top; i++) uf->connect(i, bot);
    }
    ~Percolation() {
        delete uf;
    }
    void open(int i, int j) {
        int p = get(i, j);
        data[i-1][j-1] = true;

        if (i > 1 && isOpen(i-1, j))
            uf->connect(p, get(i-1, j));
        if (i < size && isOpen(i+1, j))
            uf->connect(p, get(i+1, j));
        if (j > 1 && isOpen(i, j-1))
            uf->connect(p, get(i, j-1));
        if (j < size && isOpen(i, j+1))
            uf->connect(p, get(i, j+1));
    }
    bool isOpen(int i, int j) {
        return data[i-1][j-1];
    }
    bool isFull(int i, int j) {
        return isOpen(i, j) && uf->connected(get(i, j), top);
    }
    bool percolates() {
        return uf->connected(top, bot);
    }
private:
    int get(int i, int j) {
        if (i < 1 || i > size)
            throw "Arguments out of range";
        if (j < 1 || j > size)
            throw "Arguments out of range";
        return (i-1) * size + j - 1;
    }
};

class PercolationStats {
    vector<double> data;
    double m, s, cLo, cHi;
public:
    PercolationStats(int n, int t) {
        int total = n * n;
        for (int i = 0; i < t; i++) {
            int count = 0;
            Percolation *p = new Percolation(n);
            while (!p->percolates()) {
                int i = rand() % n + 1;
                int j = rand() % n + 1;
                if (!p->isOpen(i, j)) {
                    p->open(i, j);
                    count += 1;
                }
            }
            delete p;
            data.push_back(((double) count) / total);
        }
        m = Stats::mean(data);
        s = Stats::stddevp(data, m);
        cLo = m - 1.96 * s;
        cHi = m + 1.96 * s;
    }
    double mean () {
        return m;
    }
    double stddev() {
        return s;
    }
    double confidenceLo() {
        return cLo;
    }
    double confidenceHi() {
        return cHi;
    }
};

int main(int argc, char* args[])
{
    if (argc == 3) {
        int n = atoi(args[1]);
        int t = atoi(args[2]);
        PercolationStats ps (n, t);
        cout << "mean                    = " << ps.mean() << endl;
        cout << "stddev                  = " << ps.stddev() << endl;
        cout << "95% confidence interval = (" << ps.confidenceLo() << ", " << ps.confidenceHi() << ")" << endl;
    }
    return 0;
}
