#! /usr/bin/env ruby

module Stats
  def self.mean xs
    xs.reduce(:+) / xs.length
  end

  def self.varp xs, avg
    sum = xs.reduce(0) do |acc, x|
      acc + (x - avg) * (x - avg)
    end
    sum / xs.length
  end

  def self.stddevp xs, avg
    Math.sqrt(varp(xs, avg))
  end
end

class UnionFind
  def initialize n
    @id = Array.new(n) { |i| i }
    @sz = Array.new(n, 1)
  end

  def connected? p, q
    root(p) == root(q)
  end

  def union p, q
    i = root(p)
    j = root(q)
    if @sz[i] < @sz[j]
      @id[i] = j
      @sz[j] += 1
    else
      @id[j] = i
      @sz[i] += 1
    end
  end

  private
  def root i
    while i != @id[i]
      @id[i] = @id[@id[i]]
      i = @id[i]
    end
    i
  end
end

class Percolation
  def initialize n
    @size = n
    @open = Array.new(@size) { |i| Array.new(@size) }
    @top = n * n
    @bot = @top + 1
    @uf = UnionFind.new(@top+2)

    0.upto(@size-1) { |i| @uf.union(i, @top) }
    (@top-@size).upto(@top-1) { |i| @uf.union(i, @bot) }
  end

  def open i, j
    p = get(i, j)
    @open[i-1][j-1] = true

    @uf.union(p, get(i-1, j)) if i > 1 && open?(i-1,j)
    @uf.union(p, get(i+1, j)) if i < @size && open?(i+1,j)
    @uf.union(p, get(i, j-1)) if j > 1 && open?(i,j-1)
    @uf.union(p, get(i, j+1)) if j < @size && open?(i,j+1)
  end

  def open? i, j
    @open[i-1][j-1]
  end

  def full? i, j
    open?(i,j) && @uf.connected?(get(i, j), @top)
  end

  def percolates?
    @uf.connected?(@top, @bot)
  end

  private
  def get i, j
    raise ArgumentError, "Arguments must be between 1 and #{@size}" if i < 1 || i > @size
    raise ArgumentError, "Arguments must be between 1 and #{@size}" if j < 1 || j > @size
    (i-1) * @size + j - 1
  end
end

class PercolationStats
  def initialize n, t
    @data = Array.new(t) do |i|
      c = 0
      p = Percolation.new(n)
      while not p.percolates?
        i = Random.rand(n) + 1
        j = Random.rand(n) + 1

        unless p.open?(i, j)
          p.open(i, j)
          c += 1
        end
      end

      c.to_f / (n*n)
    end
    @mean = Stats::mean(@data)
    @stddev = Stats::stddevp(@data, @mean)
    @low = @mean - 1.96 * @stddev
    @high = @mean + 1.96 * @stddev
  end

  def mean
    @mean
  end

  def stddev
    @stddev
  end

  def confidenceEndpoints
    return @low, @high
  end
end

if __FILE__ == $0
  n = ARGV[0].to_i
  t = ARGV[1].to_i

  ps = PercolationStats.new(n, t)
  puts "mean                    = #{ps.mean}"
  puts "stddev                  = #{ps.stddev}"
  puts "95% confidence interval = #{ps.confidenceEndpoints}"
end
