#! /usr/bin/env python

import math
import sys
from random import randint

def mean(xs):
    return sum(xs) / len(xs)

def varp(xs, avg):
    s = 0
    for x in xs:
        s += (x - avg) * (x - avg)
    return s / len(xs)

def stddevp(xs, avg):
    return math.sqrt(varp(xs, avg))

class UnionFind:
    def __init__(self, n):
        self.id = [i for i in range(n)]
        self.sz = [1] * n

    def is_connected(self, p, q):
        return self.root(p) == self.root(q)

    def union(self, p, q):
        i = self.root(p)
        j = self.root(q)
        if self.sz[i] < self.sz[j]:
            self.id[i] = j
            self.sz[j] += 1
        else:
            self.id[j] = i
            self.sz[i] += 1

    def root(self, i):
        while i != self.id[i]:
            self.id[i] = self.id[self.id[i]]
            i = self.id[i]
        return i

class Percolation:
    def __init__(self, n):
        self.size = n
        self.data = [[False] * n for i in range(n)]
        self.top = n * n
        self.bot = self.top + 1
        self.uf = UnionFind(self.top+2)

        for i in range(0, self.size):
            self.uf.union(i, self.top)
        for i in range(self.top-self.size, self.top):
            self.uf.union(i, self.bot)

    def open(self, i, j):
        p = self.get(i, j)
        self.data[i-1][j-1] = True

        if (i > 1) and self.is_open(i-1,j):
            self.uf.union(p, self.get(i-1, j))
        if (i < self.size) and self.is_open(i+1,j):
            self.uf.union(p, self.get(i+1, j))
        if (j > 1) and self.is_open(i,j-1):
            self.uf.union(p, self.get(i, j-1))
        if (j < self.size) and self.is_open(i,j+1):
            self.uf.union(p, self.get(i, j+1))

    def is_open(self, i, j):
        return self.data[i-1][j-1]

    def is_full(self, i, j):
        return self.is_open(i,j) and self.uf.is_connected(self.get(i, j), self.top)

    def percolates(self):
        return self.uf.is_connected(self.top, self.bot)

    def get(self, i, j):
        if i < 1 or i > self.size:
            raise ValueError("Arguments must be between 1 and %d" % self.size)
        if j < 1 or j > self.size:
            raise ValueError("Arguments must be between 1 and %d" % self.size)
        return (i-1) * self.size + j - 1

class PercolationStats:
    def __init__(self, n, t):
        self.data = []
        for k in range(t):
            c = 0
            p = Percolation(n)
            while not p.percolates():
                i = randint(1,n)
                j = randint(1,n)

                if not p.is_open(i, j):
                    p.open(i, j)
                    c += 1
            self.data.append(c / (n*n))

        self.m = mean(self.data)
        self.s = stddevp(self.data, self.m)
        self.low = self.m - 1.96 * self.s
        self.high = self.m + 1.96 * self.s

    def mean(self):
        return self.m

    def stddev(self):
        return self.s

    def confidenceEndpoints(self):
        return (self.low, self.high)

if __name__ == '__main__':
    n = int(sys.argv[1])
    t = int(sys.argv[2])

    ps = PercolationStats(n, t)
    print("mean                    = %f" % ps.mean())
    print("stddev                  = %f" % ps.stddev())
    print("95%% confidence interval = (%f, %f)" % ps.confidenceEndpoints())
