package main

import (
  "errors"
  "fmt"
  "math"
  "math/rand"
  "os"
  "strconv"
)

func mean(xs []float64) float64 {
  sum := 0.0
  size := len(xs)
  for i := 0; i < size; i++ {
    sum += xs[i]
  }
  return sum / float64(size)
}

func varp(xs []float64, avg float64) float64 {
  sum := 0.0
  size := len(xs)
  for i := 0; i < size; i++ {
    sum += (xs[i] - avg) * (xs[i] - avg)
  }
  return sum / float64(size)
}

func stddevp(xs []float64, avg float64) float64 {
  return math.Sqrt(varp(xs, avg))
}

type UnionFind struct {
  id []int
  sz []int
}

func NewUnionFind(n int) UnionFind {
  uf := UnionFind {id: make([]int, n), sz: make([]int, n)}
  for i := 0; i < n; i++ {
    uf.id[i] = i
  }
  for i := 0; i < n; i++ {
    uf.sz[i] = 1
  }
  return uf
}

func (uf *UnionFind) Union(i, j int) {
  p := uf.root(i)
  q := uf.root(j)

  if uf.sz[p] < uf.sz[q] {
    uf.id[p] = uf.id[q]
    uf.sz[q] += 1
  } else {
    uf.id[q] = uf.id[p]
    uf.sz[p] += 1
  }
}

func (uf *UnionFind) Connected(i, j int) bool {
  return uf.root(i) == uf.root(j)
}

func (uf *UnionFind) root(i int) int {
  for i != uf.id[i] {
    uf.id[i] = uf.id[uf.id[i]]
    i = uf.id[i]
  }
  return i
}

type Percolation struct {
  UnionFind
  data [][]bool
  size int
  top int
  bot int
}

func NewPercolation(n int) Percolation {
  x := n * n
  p := Percolation { NewUnionFind(x+2), make([][]bool, n), n, x, x+1 }
  for i := 0; i < p.size; i++ {
    p.data[i] = make([]bool, n)
    for j := 0; j < p.size; j++ {
      p.data[i][j] = false
    }
  }
  for i := 0; i < p.size; i++ {
    p.Union(i, p.top)
  }
  for i := p.top - p.size; i < p.top; i++ {
    p.Union(i, p.bot)
  }

  return p
}

func (p *Percolation) Open(i, j int) {
  x, _ := p.get(i, j)
  p.data[i-1][j-1] = true

  if (i > 1 && p.IsOpen(i-1, j)) {
    y, _ := p.get(i-1, j)
    p.Union(x, y)
  }
  if (i < p.size && p.IsOpen(i+1, j)) {
    y, _ := p.get(i+1, j)
    p.Union(x, y)
  }
  if (j > 1 && p.IsOpen(i, j-1)) {
    y, _ := p.get(i, j-1)
    p.Union(x, y)
  }
  if (j < p.size && p.IsOpen(i, j+1)) {
    y, _ := p.get(i, j+1)
    p.Union(x, y)
  }
}

func (p *Percolation) IsOpen(i, j int) bool {
  return p.data[i-1][j-1]
}

func (p *Percolation) IsFull(i, j int) bool {
  x, _ := p.get(i, j)
  return p.IsOpen(i, j) && p.Connected(x, p.top)
}

func (p *Percolation) Percolates() bool {
  return p.Connected(p.top, p.bot)
}

func (p *Percolation) get(i, j int) (int, error) {
  if i < 1 || i > p.size {
    return 0, errors.New("Arguments out of range")
  }
  if j < 1 || j > p.size {
    return 0, errors.New("Arguments out of range")
  }
  return (i-1) * p.size + j - 1, nil
}

type PercolationStats struct {
  data []float64
  mean float64
  stddev float64
  confidenceLo float64
  confidenceHi float64
}

func NewPercolationStats(n, t int) PercolationStats {
  total := n * n
  data := make([]float64, t)
  for k := 0; k < t; k++ {
    c := 0
    p := NewPercolation(n)
    for !p.Percolates() {
      i := rand.Intn(n) + 1
      j := rand.Intn(n) + 1
      if !p.IsOpen(i, j) {
        p.Open(i, j)
        c += 1
      }
    }

    data[k] = float64(c) / float64(total)
  }

  m := mean(data)
  s := stddevp(data, m)
  ps := PercolationStats { data, m, s, (m - 1.96 * s), (m + 1.96 * s) }

  return ps
}

func (ps *PercolationStats) Mean() float64 {
  return ps.mean
}

func (ps *PercolationStats) Stddev() float64 {
  return ps.stddev
}

func (ps *PercolationStats) ConfidenceEndpoints() (float64, float64) {
  return ps.confidenceLo, ps.confidenceHi
}

func main() {
  argc := len(os.Args)

  if argc == 3 {
    n, _ := strconv.Atoi(os.Args[1])
    t, _ := strconv.Atoi(os.Args[2])

    ps := NewPercolationStats(n, t)

    fmt.Printf("mean                    = %f\n", ps.Mean())
    fmt.Printf("stddev                  = %f\n", ps.Stddev())
    x, y := ps.ConfidenceEndpoints()
    fmt.Printf("95%% confidence interval = (%f, %f)\n", x, y)
  }
}
